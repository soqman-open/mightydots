﻿
using UnityEngine;
using UnityEngine.Events;

public class GameController : Singleton<GameController>
{
	[Header("Game events")]
	public UnityEvent OnGameStartEvent;
	public UnityEvent OnGameStopEvent;
	
	[Header("Game settings")]
	public int MapMinSize=7;
	public int MapMaxSize=12;
	public int UnitsMaxCount=5;
	public float Speed = 1;

	[Header("Game managers")] 
	public Area Area;
	public TcpConnecterClient ConnecterClient;
	
	private void GameStartTrigger()
	{
		OnGameStartEvent.Invoke();
	}
	
	public void GameStopTrigger()
	{
		OnGameStopEvent.Invoke();
	}

	public void Move(Unit unit)
	{
		ConnecterClient.SendUnitMove(unit);
	}

	public void GameEnter()
	{
		if (!CheckSettings())
		{
			Debug.Log("Wrong settings");
			return;
		}
		var args=new int[3]{MapMinSize,MapMaxSize,UnitsMaxCount};
		var size = ConnecterClient.CreateMapAndGetSize(args);
		if (size >= MapMinSize && size <= MapMaxSize)
		{
			Area.CreateNodes(size);
		}
	}

	public void GameStart()
	{
		GameStartTrigger();
		if(Area.Nodes.Length>0)
		Area.CreateUnits(ConnecterClient.GetUnitsArray());
		else
		{
			Debug.Log("Game not started properly");
		}
	}

	private bool CheckSettings()
	{
		return !(MapMinSize >= MapMaxSize || UnitsMaxCount <= 0 || MapMinSize <= 0 ||
		         MapMinSize * MapMinSize <= UnitsMaxCount);
	}
	
}
