﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class UdpReceive : MonoBehaviour {

    private Thread _receiveThread;
    private UdpClient _client;
    public int Port=10000; 
    private bool _active;
    public static event WithUnitsDelegate OnDataRecieveEvent;
    public delegate void WithUnitsDelegate(List<Unit> units);
    private bool _toMainThreadInvoke;
    private byte[] _data;

    public void Start()
    {
        Debug.Log("Udp listener started");
        _active = true;
        _client = new UdpClient(Port);
        _receiveThread = new Thread(ReceiveData) {IsBackground = true};
        _receiveThread.Start(); 
    }

    private void Update()
    {
        if (!_toMainThreadInvoke) return;
        if (OnDataRecieveEvent != null) OnDataRecieveEvent.Invoke(ParseUnit(_data));
        _toMainThreadInvoke = false;
    }
    private void OnDestroy()
    {
        Stop();
    }

    private void Stop()
    {
        _active = false;
        _receiveThread.Interrupt();
        _client.Close();
        Debug.Log("Stopped");
    }

    private void ReceiveData()
    {
        var remoteIpEndPoint = new IPEndPoint(IPAddress.Any, Port);
        while (_active)
        {
            try
            {
                _data = _client.Receive(ref remoteIpEndPoint);
                _toMainThreadInvoke = true;
            }
            catch (Exception err)
            {
                Debug.Log(err);
            }
        }
    }

    private List<Unit> ParseUnit(byte[] bytes)
    {
        return Utils.ByteArrayToList(bytes);
    }
}