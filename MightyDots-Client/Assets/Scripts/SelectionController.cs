﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectionController: MonoBehaviour
{
	public Image Selection;
	public Area Area;
	private Vector2 _mouseStartPosition;

	public void OnBeginDrag(BaseEventData data)
	{
		if (((PointerEventData) data).button == PointerEventData.InputButton.Left) BeginDrag();
	}
	
	public void OnDrag(BaseEventData data)
	{
		if (((PointerEventData) data).button == PointerEventData.InputButton.Left) DrawArea();
	}

	public void OnEndDrag(BaseEventData data)
	{
		if (((PointerEventData) data).button == PointerEventData.InputButton.Left)
		EndDrag();
	}

	public void OnClick(BaseEventData data)
	{
		switch (((PointerEventData) data).button)
		{
			case PointerEventData.InputButton.Left:
				Click();
				break;
			case PointerEventData.InputButton.Right:
				Go();
				break;
			case PointerEventData.InputButton.Middle:
				break;
			default:
				throw new ArgumentOutOfRangeException();
		}
	}

	private void Click()
	{
		if (!Input.GetKey(KeyCode.LeftControl))Deselect();
		RaycastHit hit;
		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (!Physics.Raycast(ray, out hit) && hit.collider == null) return;
		var ub = hit.collider.GetComponent<UnitBehaviour>();
		if (ub == null) return;
		if (Input.GetKey(KeyCode.LeftControl))ub.OnTrigger();
		else ub.OnSelect();
	}

	private void Go()
	{
		RaycastHit hit;
		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out hit) && hit.collider != null)
		{
			var nodeBehaviour = hit.collider.GetComponent<NodeBehaviour>();
			if (nodeBehaviour!=null)
			{
				foreach (var unit in Area.Units)
				{
					if(unit.IsSelected)unit.Move(nodeBehaviour.GetX(),nodeBehaviour.GetY());
				}
			}
		}
		Deselect();
	}

	private void DrawArea()
	{
		if (_mouseStartPosition.x <= Input.mousePosition.x && _mouseStartPosition.y <= Input.mousePosition.y)
		{
			Selection.rectTransform.pivot=Vector2.zero;
		}
		else if (_mouseStartPosition.x <= Input.mousePosition.x && _mouseStartPosition.y > Input.mousePosition.y)
		{
			Selection.rectTransform.pivot=Vector2.up;
		}
		else if (_mouseStartPosition.x > Input.mousePosition.x && _mouseStartPosition.y <= Input.mousePosition.y)
		{
			Selection.rectTransform.pivot=Vector2.right;
		}
		else if (_mouseStartPosition.x > Input.mousePosition.x && _mouseStartPosition.y > Input.mousePosition.y)
		{
			Selection.rectTransform.pivot=Vector2.one;
		}
		Selection.rectTransform.sizeDelta=new Vector2(Math.Abs(Input.mousePosition.x-_mouseStartPosition.x),Math.Abs(Input.mousePosition.y-_mouseStartPosition.y));
	}

	private void BeginDrag()
	{
		if (!Input.GetKey(KeyCode.LeftControl))Deselect();
		Selection.rectTransform.position = Input.mousePosition;
		_mouseStartPosition=Input.mousePosition;
	}

	private void EndDrag()
	{
		Select();
		Selection.rectTransform.sizeDelta=Vector2.zero;
	}
	
	private void Select()
	{
		var rectT= Selection.rectTransform;
		var rect=new Rect(rectT.rect.x+rectT.position.x,rectT.rect.y+rectT.position.y,rectT.sizeDelta.x,rectT.sizeDelta.y);
		foreach (var selectable in Area.Units)
		{
			if (Input.GetKey(KeyCode.LeftControl))
			{
				if(selectable.CheckIsOverlapped(rect))selectable.OnTrigger();
			}
			else
			{
				if(selectable.CheckIsOverlapped(rect))selectable.OnSelect();
			}
		}
	}
	
	private void Deselect()
	{
		foreach (var selectable in Area.Units)
		{
			selectable.OnDeselect();
		}
	}
}
