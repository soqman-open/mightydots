﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

public class TcpConnecterClient : MonoBehaviour
{
    public string Ip;
    public int Port;
    private TcpClient _client;
    private NetworkStream _tcpStream;
    private const string Getsizeofmap = "GMS";
    private const string Getunitsarray = "GUA";
    private const string Takeunitmoving = "TUM";


    private void Start()
    {
        _client = new TcpClient();
    }

    public void Connect()
    {
        if (_client.Connected) return;
        _client.Connect(Ip,Port);
        _tcpStream = _client.GetStream();
        _tcpStream.ReadTimeout = 1000;
        Debug.Log(_client.Connected ? "Connect successful" : "Connect failed");
    }

    private void Stop()
    {
        if(_tcpStream!=null)_tcpStream.Close();
        if(_client!=null && _client.Connected)_client.Close();
        Debug.Log("TCP connection closed");
    }

    private void OnDestroy()
    {
        Stop();
    }

    public int CreateMapAndGetSize(int[] args)
    {
        var argsbyte = args.SelectMany(BitConverter.GetBytes).ToArray();
        var size=BitConverter.ToInt32(SendData(Getsizeofmap,argsbyte),0);
        return size;
    }
    
    public List<Unit> GetUnitsArray()
    {
        var bytes = SendData(Getunitsarray, null);
        var list=Utils.ByteArrayToList(bytes);
        return list;
    }

    public void SendUnitMove(Unit unit)
    {
        var bytes = Utils.UnitToByteArray(unit);
        var message = SendData(Takeunitmoving,bytes);
        Debug.Log(Encoding.ASCII.GetString(message));
    }

    private byte[] SendData(string cmd,byte[] args)
    {
        if (_client == null || !_client.Connected || _tcpStream == null)
        {
            Debug.Log("Ошибка сети");
            return null;
        }
        try
        {
            var data = new ArrayList();
            data.AddRange(Encoding.ASCII.GetBytes(cmd));
            if(args!=null)data.AddRange(args);
            _tcpStream.Write((byte[])data.ToArray(typeof(byte)), 0, data.Count);
            var bytes = new byte[_client.ReceiveBufferSize];
            _tcpStream.Read(bytes, 0, _client.ReceiveBufferSize);
            return bytes;
        }
        catch (SocketException err)
        {
            Debug.Log(err);
            return null;
        }
    }
    
    
}

