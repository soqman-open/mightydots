﻿using UnityEngine;

public interface ISelectable
{

	void OnSelect();
	void OnDeselect();
	void OnTrigger();
	bool CheckIsOverlapped(Rect rect);

}
