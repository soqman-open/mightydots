﻿using UnityEngine;

public class NodeBehaviour : MonoBehaviour
{
	public int Interval;
	private int _x;
	private int _y;
	
	public void SetPosition(int x, int y)
	{
		_x=x; _y=y;
		transform.position=new Vector3(_x*Interval,1,_y*Interval);
	}

	public int GetX()
	{
		return _x;
	}
	
	public int GetY()
	{
		return _y;
	}
}
