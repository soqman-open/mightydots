﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Area : MonoBehaviour
{	
	[Header("Prefabs")]
	public GameObject NodePrefab;
	public GameObject UnitPrefab;
	[Header("Objects")] 
	public List<UnitBehaviour> Units = new List<UnitBehaviour>();
	public NodeBehaviour[,] Nodes;
	[Header("Managers")] public UdpReceive UdpReceive;

	private void Start()
	{
		UdpReceive.OnDataRecieveEvent += UpdateArea;
	}

	private void OnDestroy()
	{
		UdpReceive.OnDataRecieveEvent -= UpdateArea;
	}

	public void CreateNodes(int n)
	{
		Nodes = new NodeBehaviour[n,n];
		Debug.Log("Area creating");
		for (var i = 0; i < n; i++)
		{
			for (var j = 0; j < n; j++)
			{
				var nodeBehaviour = Instantiate(NodePrefab, transform).GetComponent<NodeBehaviour>();
				nodeBehaviour.SetPosition(i,j);
				Nodes[i,j]=nodeBehaviour;
			}
		}
	}

	public void CreateUnits(List<Unit> units)
	{
		if (units.Count == 0) throw new ArgumentException("Value cannot be an empty collection.", "units");
		Debug.Log("Units creating");
		foreach (var unit in units)
		{
			var unitBehaviour = Instantiate(UnitPrefab,transform).GetComponent<UnitBehaviour>();
			unitBehaviour.AttachedUnit = unit;
			unitBehaviour.AttachArea(this);
			Units.Add(unitBehaviour);
			unitBehaviour.UpdatePosition();
		}
	}

	private void UpdateArea(List<Unit> units)
	{
		Debug.Log("updated");
		foreach (var unit in Units)
		{
			unit.AttachedUnit = units.Find(x => x.Id == unit.AttachedUnit.Id);
			unit.UpdatePosition();
		}
	}
}