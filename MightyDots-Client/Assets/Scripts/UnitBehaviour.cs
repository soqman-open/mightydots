﻿using UnityEngine;
using UnityEngine.Events;

public class UnitBehaviour : MonoBehaviour, ISelectable
{
	private Renderer _renderer;
	private bool _selected;
	public Unit AttachedUnit;
	public UnityEvent OnMoveEvent;
	public UnityEvent OnStopEvent;
	public UnityEvent OnSelectedEvent;
	public UnityEvent OnDeselectedEvent;
	private Area _area;
	private UnityEngine.UI.Text _text;

	public bool IsSelected
	{
		get { return _selected; }
	}

	public bool IsMoving
	{
		get { return AttachedUnit.IsMoving; }
	}

	public void OnSelect()
	{
		_selected = true;
		OnSelectedEvent.Invoke();
	}

	public void OnDeselect()
	{
		_selected = false;
		OnDeselectedEvent.Invoke();
	}

	public void OnTrigger()
	{
		if (_selected) OnDeselect();
		else OnSelect();
	}

	public bool CheckIsOverlapped(Rect rect)
	{
		Vector2 screenSpacePos = Camera.main.WorldToScreenPoint(transform.position);
		return rect.Contains(screenSpacePos);
	}

	public void Move(int x, int y)
	{
		AttachedUnit.IsMoving = true;
		AttachedUnit.TargetX = x;
		AttachedUnit.TargetY = y;
		GameController.Instance.Move(AttachedUnit);
		OnMoveEvent.Invoke();
	}

	public void Stop()
	{
		OnStopEvent.Invoke();
	}

	public void UpdatePosition()
	{
		transform.SetPositionAndRotation(_area.Nodes[AttachedUnit.X,AttachedUnit.Y].transform.position,Quaternion.identity);
		if (!IsMoving) OnStopEvent.Invoke();
	}

	public void AttachArea(Area area)
	{
		_area = area;
		GetComponentInChildren<UnityEngine.UI.Text>().text = AttachedUnit.Id.ToString();
	}
}
