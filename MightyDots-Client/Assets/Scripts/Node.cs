﻿using System;

    [Serializable]
    public class Node
    {
        private bool _isEngaged;
        public bool IsEngaged
        {
            get { return _isEngaged; }
            set { _isEngaged = value; }
        }
    }
