﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public static class Utils {

	public static List<Unit> ByteArrayToList(byte[] bytesUnit)
	{
		using (var memStream = new MemoryStream())
		{
			var binForm = new BinaryFormatter();
			binForm.Binder = new TypeOnlyBinder();
			memStream.Write(bytesUnit, 0, bytesUnit.Length);
			memStream.Seek(0, SeekOrigin.Begin);
			var array = (Unit[])binForm.Deserialize(memStream);
			return new List<Unit>(array);
		}
	}
	
	public static byte[] UnitToByteArray(Unit unit)
	{
		var bf = new BinaryFormatter();
		using (var ms = new MemoryStream())
		{
			bf.Binder = new TypeOnlyBinder();
			bf.Serialize(ms, unit);
			return ms.ToArray();
		}
	}
	
}
public class TypeOnlyBinder : SerializationBinder
{
	public override Type BindToType(string assemblyName, string typeName)
	{
		return Type.GetType(typeName);
	}
}

