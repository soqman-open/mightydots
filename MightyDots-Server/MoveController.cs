﻿using System;
using System.Collections.Generic;
using System.Threading;

public class MoveController
{
    private List<Unit> _units;
    private Node[,] _nodes;
    private int _culldown = 500;
    private bool _active;
    private Thread _sleepyThread;
    public static event NoneDelegate OnUpdateMap;
    public delegate void NoneDelegate();
    private object _threadLock = new object();
    private bool _isPaused;

    public MoveController(Map map)
    {
        _units = map.Units;
        _nodes = map.Nodes;
        _active = true;
        _sleepyThread = new Thread(Sleeper) { IsBackground = true };
        _sleepyThread.Start();
    }

    private void Sleeper()
    {
        while (_active)
        {
            Thread.Sleep(_culldown);
            MoveAllUnits();
            lock (_threadLock)
            {
                while (_isPaused)
                {
                    Monitor.Wait(_threadLock);
                }
            }
        }
    }

    private void Resume()
    {
        lock (_threadLock)
        {
            _isPaused = false;
            Monitor.Pulse(_threadLock);
        }
    }

    public void MoveInit(Unit unit)
    {
        var i = _units.IndexOf(_units.Find(x => x.Id == unit.Id));
        _units[i] = unit;
        if (_isPaused)
        {
            Resume();
        }
    }

    private void MoveAllUnits()
    {
        bool isMovingAll = false;
        DefineWhoNeedMove();
        for (int i = 0; i < _units.Count; i++)
        {
            Unit unit = _units[i];
            if (unit.IsMoving)
            {
                MoveBehavior(unit);
                if (!OnTarget(unit)) isMovingAll = true;
            }
        }
        if (!isMovingAll) _isPaused = true;
        OnUpdateMap.Invoke();
    }

    private void MoveBehavior(Unit unit)
    {
        var currentDirection = unit.Direction;
        unit.IsMoving = false;
        TurnToTargetDirectionStraight(unit);
        if (IsForwardNodeEmpty(unit))
        {
            StepForward(unit);
        }
        else
        {
            if (currentDirection == unit.Direction || currentDirection == LookDirection.Empty)
            {
                TurnToTargetDirectionSide(unit);
            }
            else
            {
                unit.Direction = currentDirection;
            }
            if (IsForwardNodeEmpty(unit))
            {
                StepForward(unit);
            }
            else
            {
                TurnBack(unit);
                if (IsForwardNodeEmpty(unit))
                {
                    StepForward(unit);
                }
            }
        }
        unit.IsMoving = !OnTarget(unit);
    }

    private void StepForward(Unit unit)
    {
        _nodes[unit.X, unit.Y].EngagedUnit = null;
        switch (unit.Direction)
        {
            case LookDirection.Down:
                unit.Y--;
                break;
            case LookDirection.Left:
                unit.X--;
                break;
            case LookDirection.Up:
                unit.Y++;
                break;
            case LookDirection.Right:
                unit.X++;
                break;
            case LookDirection.Empty:
                break;
            default:
                break;
        }
        _nodes[unit.X, unit.Y].EngagedUnit = unit;
    }

    private void DefineWhoNeedMove()
    {
        foreach (var unit in _units)
        {
            if (OnTarget(unit))
            {
                unit.IsMoving = false;
            }
            else if (_nodes[unit.TargetX, unit.TargetY].IsEngaged && OnTarget(_nodes[unit.TargetX, unit.TargetY].EngagedUnit))
            {
                for (int i = 1; i <= GetFarestDistance(unit); i++)
                {
                    if (GetFarestDistance(unit) == i)
                    {
                        if (GetDistance(unit.X, unit.TargetX) == GetDistance(unit.Y, unit.TargetY) && (!_nodes[(unit.TargetX-unit.X)/GetDistance(unit.X,unit.TargetX)+unit.X,unit.Y].IsEngaged || !_nodes[unit.X, (unit.TargetY - unit.Y) / GetDistance(unit.Y, unit.TargetY) + unit.Y].IsEngaged)) {
                            unit.IsMoving = true;
                            break;
                        }
                        unit.IsMoving = false;
                        unit.TargetX = unit.X;
                        unit.TargetY = unit.Y;
                        break;
                    }
                    else if (IsAroundHaveEmpty(i, unit.TargetX, unit.TargetY))
                    {
                        unit.IsMoving = true;
                        break;
                    }
                }
            }
            else unit.IsMoving = true;
        }
    }

    private int GetFarestDistance(Unit unit)
    {
        return GetDistance(unit.X, unit.TargetX) >= GetDistance(unit.Y, unit.TargetY) ? GetDistance(unit.X, unit.TargetX) : GetDistance(unit.Y, unit.TargetY);
    }

    private bool OnTarget(Unit unit)
    {
        return (unit.X == unit.TargetX && unit.Y == unit.TargetY);
    }

    private bool IsAroundHaveEmpty(int distance, int x, int y)
    {
        for (var i = -distance; i <= distance; i++)
        {
            if (x + i < 0 || x + i >= _nodes.GetLength(0))continue;
            for (var j = -distance; j <= distance; j++)
            {
                if (y + j < 0 || y + j >= _nodes.GetLength(1)) continue;
                if (distance == Math.Abs(i) || distance == Math.Abs(j))
                {
                    if (!_nodes[x + i, y + j].IsEngaged) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private bool IsForwardNodeEmpty(Unit unit)
    {
        switch (unit.Direction)
        {
            case LookDirection.Down:
                return IsEmptyNode(unit.X, unit.Y - 1);
            case LookDirection.Left:
                return IsEmptyNode(unit.X - 1, unit.Y);
            case LookDirection.Up:
                return IsEmptyNode(unit.X, unit.Y + 1);
            case LookDirection.Right:
                return IsEmptyNode(unit.X + 1, unit.Y);
            case LookDirection.Empty:
                return true;
            default:
                return true;
        }
    }

    private void TurnToTargetDirectionSide(Unit unit)
    {
        switch (unit.Direction)
        {
            case LookDirection.Down:
                if (unit.TargetX < unit.X) TurnRight(unit);
                else if (unit.TargetX > unit.X) TurnLeft(unit);
                else TurnRight(unit);
                break;
            case LookDirection.Left:
                if (unit.TargetY < unit.Y) TurnLeft(unit);
                else if (unit.TargetY > unit.Y) TurnRight(unit);
                else TurnRight(unit);
                break;
            case LookDirection.Up:
                if (unit.TargetX < unit.X) TurnLeft(unit);
                else if (unit.TargetX > unit.X) TurnRight(unit);
                else TurnRight(unit);
                break;
            case LookDirection.Right:
                if (unit.TargetY < unit.Y) TurnRight(unit);
                else if (unit.TargetY > unit.Y) TurnLeft(unit);
                else TurnRight(unit);
                break;
            case LookDirection.Empty:
                unit.Direction = LookDirection.Empty;
                break;
        }
    }

    private void TurnToTargetDirectionStraight(Unit unit)
    {
        if (DistanceFarToTarger(unit) == DistanceFar.Y)
        {
            if (unit.TargetY < unit.Y) unit.Direction = LookDirection.Down;
            if (unit.TargetY > unit.Y) unit.Direction = LookDirection.Up;
        }
        else if (DistanceFarToTarger(unit) == DistanceFar.X)
        {
            if (unit.TargetX < unit.X) unit.Direction = LookDirection.Left;
            if (unit.TargetX > unit.X) unit.Direction = LookDirection.Right;
        }
        else if (unit.Direction == LookDirection.Right || unit.Direction == LookDirection.Left)
        {
            if (unit.TargetX < unit.X) unit.Direction = LookDirection.Left;
            if (unit.TargetX > unit.X) unit.Direction = LookDirection.Right;
            

        }
        else if (unit.Direction == LookDirection.Up || unit.Direction == LookDirection.Down)
        {
            if (unit.TargetX < unit.X) unit.Direction = LookDirection.Left;
            if (unit.TargetX > unit.X) unit.Direction = LookDirection.Right;
        }
        else if (unit.Direction == LookDirection.Empty) {
            if (unit.TargetY < unit.Y) unit.Direction = LookDirection.Down;
            if (unit.TargetY > unit.Y) unit.Direction = LookDirection.Up;
        }
    }

    private void TurnLeft(Unit unit)
    {
        switch (unit.Direction)
        {
            case LookDirection.Down:
                unit.Direction = LookDirection.Right;
                break;
            case LookDirection.Left:
                unit.Direction = LookDirection.Down;
                break;
            case LookDirection.Up:
                unit.Direction = LookDirection.Left;
                break;
            case LookDirection.Right:
                unit.Direction = LookDirection.Up;
                break;
            case LookDirection.Empty:
                unit.Direction = LookDirection.Empty;
                break;
        }
    }

    private void TurnRight(Unit unit)
    {
        switch (unit.Direction)
        {
            case LookDirection.Down:
                unit.Direction = LookDirection.Left;
                break;
            case LookDirection.Left:
                unit.Direction = LookDirection.Up;
                break;
            case LookDirection.Up:
                unit.Direction = LookDirection.Right;
                break;
            case LookDirection.Right:
                unit.Direction = LookDirection.Down;
                break;
            case LookDirection.Empty:
                unit.Direction = LookDirection.Empty;
                break;
        }
    }

    private void TurnBack(Unit unit)
    {
        switch (unit.Direction)
        {
            case LookDirection.Down:
                unit.Direction = LookDirection.Up;
                break;
            case LookDirection.Left:
                unit.Direction = LookDirection.Right;
                break;
            case LookDirection.Up:
                unit.Direction = LookDirection.Down;
                break;
            case LookDirection.Right:
                unit.Direction = LookDirection.Left;
                break;
            case LookDirection.Empty:
                unit.Direction = LookDirection.Empty;
                break;
        }
    }

    private DistanceFar DistanceFarToTarger(Unit unit)
    {
        var delayX = GetDistance(unit.X, unit.TargetX);
        var delayY = GetDistance(unit.Y, unit.TargetY);
        var farIs = DistanceFar.Same;
        if (delayX > delayY) farIs = DistanceFar.X;
        else if (delayY > delayX) farIs = DistanceFar.Y;
        return farIs;
    }

    private int GetDistance(int i, int target)
    {
        return Math.Abs(i - target);
    }

    private bool IsEmptyNode(int x, int y)
    {

        if (x < 0 || y < 0 || x > _nodes.GetLength(0) - 1 || y > _nodes.GetLength(1) - 1) return false;
        if (_nodes[x, y].IsEngaged)
        {
            var u = _nodes[x, y].EngagedUnit;
            if (_units[u.Id].IsMoving)
            {
                MoveBehavior(_units[u.Id]);
            }
        }
        return !_nodes[x, y].IsEngaged;
    }
}

enum DistanceFar { X, Y, Same }