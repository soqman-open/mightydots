﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

class TcpReceiver
{
    private TcpListener _listener;
    private TcpClient _client;
    private Thread _receiveThread;
    NetworkStream stream;
    public int Port;
    public string Ip;
    private bool _active;
    private const string Getsizeofmap = "GMS";
    private const string Getunitsarray = "GUA";
    private const string Takeunitmoving = "TUM";
    public static event WithByteArray OnGetSettings;
    public static event NoneDelegate OnRequestUnits;
    public static event WithByteArray OnMoveUnits;
    public delegate void NoneDelegate();
    public delegate void WithByteArray(byte[] bytes);

    public void Start()
    {
        Console.WriteLine("Listener started");
        _active = true;
        _receiveThread = new Thread(ReceiveData) { IsBackground = true };
        _receiveThread.Start();
    }

    public void Stop()
    {
        if (_active)
        {
            _active = false;
            _listener.Stop();
            _receiveThread.Interrupt();
            Console.WriteLine("Stopped");
        }
    }

    private void ReceiveData()
    {
        try
        {
            Console.WriteLine("Ожидание подключений... ");
            IPAddress localIp = IPAddress.Parse(Ip);
            _listener = new TcpListener(localIp, Port);
            _listener.Start();
            _client = _listener.AcceptTcpClient();
            Byte[] cmd = new byte[3];
            Console.WriteLine("Подключен клиент");
            stream = _client.GetStream();
            while (_active && _client.Connected)
            {
                stream.Read(cmd, 0, cmd.Length);
                var cmdString = Encoding.ASCII.GetString(cmd, 0, cmd.Length);
                Console.WriteLine(cmdString);
                if (cmdString == Getsizeofmap)
                {
                    var bytes = new byte[12];
                    stream.Read(bytes, 0, 3 * sizeof(Int32));
                    OnGetSettings.Invoke(bytes);
                }
                if (cmdString == Getunitsarray)
                {
                    OnRequestUnits.Invoke();
                }
                if (cmdString == Takeunitmoving)
                {
                    var bytes = new byte[_client.ReceiveBufferSize];
                    stream.Read(bytes, 0, _client.ReceiveBufferSize);
                    OnMoveUnits.Invoke(bytes);
                }
            }
            stream.Close();
            _client.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
        finally
        {
            if (_listener != null)
                _listener.Stop();
        }
    }

    public void SendData(byte[] bytes)
    {
        if (stream != null)
        {
            stream.Write(bytes, 0, bytes.Length);
        }
        else Console.Write("stream is null");
    }
}



