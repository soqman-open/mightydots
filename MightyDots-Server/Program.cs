﻿using System;

class Program
{
    static void Main(string[] args)
    {
        GameManagerServer game = new GameManagerServer();
        bool active = true;
        while (active)
        {
            var command = Console.ReadLine();
            if (command.Equals("exit"))
            {
                active = false;
                game.Stop();
            }
        }
    }
}
