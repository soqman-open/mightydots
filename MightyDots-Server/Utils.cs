﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

class Utils
{
    public static byte[] UnitsToByteArray(List<Unit> obj)
    {
        var array = obj.ToArray();
        var bf = new BinaryFormatter();
        using (var ms = new MemoryStream())
        {
            bf.Binder = new TypeOnlyBinder();
            bf.Serialize(ms, array);
            return ms.ToArray();
        }
    }

    public static Unit ByteArrayToUnit(byte[] bytes)
    {
        using (var memStream = new MemoryStream())
        {
            var binForm = new BinaryFormatter();
            binForm.Binder = new TypeOnlyBinder();
            memStream.Write(bytes, 0, bytes.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            return (Unit)binForm.Deserialize(memStream);
        }
    }
}

public class TypeOnlyBinder : SerializationBinder
{
    public override Type BindToType(string assemblyName, string typeName)
    {
        return Type.GetType(typeName);
    }

    public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
    {
        assemblyName = "NA";
        typeName = serializedType.FullName;
    }
}

