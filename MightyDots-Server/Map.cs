﻿
using System;
using System.Collections.Generic;
using Random = System.Random;

public class Map
{
    public readonly int Size;
    public Node[,] Nodes;
    public readonly List<Unit> Units = new List<Unit>();

    public Map(int size, int maxNodes)
    {
        if (size <= 0 || size * size <= maxNodes || maxNodes <= 0) return;
        Size = size;
        InitNodes();
        GenerateUnits(maxNodes);
    }

    public Map(int minSize, int maxSize, int maxUnits)
    {
        if (minSize >= maxSize || maxUnits <= 0 || minSize <= 0 || minSize * minSize <= maxUnits) throw new ArgumentException("Wrong settings");
        Size = GenerateRand(minSize, maxSize);
        InitNodes();
        // GenerateUnits(GenerateRand(maxUnits));
        GenerateUnits(5);
    }

    private void InitNodes()
    {
        Nodes = new Node[Size, Size];
        for (var i = 0; i < Size; i++)
        {
            for (var j = 0; j < Size; j++)
            {
                Nodes[i, j] = new Node();
            }
        }
    }

    private void GenerateUnits(int count)
    {
        for (var i = 0; i < count; i++)
        {
            var unit = new Unit(i);
            Units.Add(unit);
            PlaceUnit(unit);
        }
    }

    private void PlaceUnit(Unit unit)
    {
        var x = 0;
        var y = 0;
        var success = false;
        while (!success)
        {
            x = new Random().Next(0, Size);
            y = new Random().Next(0, Size);
            if (!Nodes[x, y].IsEngaged) success = true;
        }
        unit.X = x;
        unit.TargetX = x;
        unit.Y = y;
        unit.TargetY = y;
        Nodes[x, y].EngagedUnit = unit;
    }

    private static int GenerateRand(int minSize, int maxSize)
    {
        return new Random().Next(minSize, maxSize);
    }

    private static int GenerateRand(int maxSize)
    {
        return new Random().Next(1, maxSize);
    }
}

