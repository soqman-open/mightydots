﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;

public class UdpSender
{
    public string Ip;
    public int Port;
    private IPEndPoint _remoteEndPoint;
    private UdpClient _client;

    public void Start()
    {
        Console.WriteLine("Sender start");
        _client = new UdpClient();
        _remoteEndPoint = new IPEndPoint(IPAddress.Parse(Ip), Port);
    }

    public void Stop()
    {
        _client.Close();
        Console.WriteLine("Stopped");
    }

    private void SendData(byte[] data)
    {
        try
        {
            _client.Send(data, data.Length, _remoteEndPoint);
        }
        catch (Exception err)
        {
            Console.WriteLine(err);
        }
    }

    public void SendUnitsUpdate(List<Unit> units)
    {
        SendData(Utils.UnitsToByteArray(units));
        Console.WriteLine("send udp");
    }
}
