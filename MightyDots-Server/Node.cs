﻿using System;

[Serializable]
public class Node
{
    private bool _isEngaged;
    private Unit _unit;
    public bool IsEngaged
    {
        get { return _isEngaged; }
    }

    public Unit EngagedUnit
    {
        get { return _unit; }
        set
        {    _unit = value;
            if (value == null)
            {
                _isEngaged = false;
            }
            else {
                _isEngaged = true;
            }
            
        }
    }
}

