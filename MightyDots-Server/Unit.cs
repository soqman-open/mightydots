﻿[System.Serializable]
public class Unit
{
    private readonly int _id;
    public bool IsMoving;
    public int X;
    public int Y;
    public int TargetX;
    public int TargetY;
    private LookDirection _lookDirection = LookDirection.Empty;

    public Unit(int id)
    {
        _id = id;
    }

    public int Id
    {
        get { return _id; }
    }

    public LookDirection Direction
    {
        get { return _lookDirection; }
        set { _lookDirection = value; }
    }

    public override string ToString()
    {
        return _id.ToString();
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        return obj.GetType() == this.GetType() && Equals((Unit)obj);
    }

    private bool Equals(Unit other)
    {
        return _id == other._id;
    }

    public override int GetHashCode()
    {
        return _id;
    }
}
