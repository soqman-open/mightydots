﻿using System;
using System.Text;

internal class GameManagerServer
{
    private UdpSender udpSender;
    private TcpReceiver tcpReciever;
    private Map map;
    private MoveController moveController;

    public GameManagerServer()
    {
        AppDomain.CurrentDomain.ProcessExit += new EventHandler(Stop);
        Init();
    }

    private void Init()
    {
        udpSender = new UdpSender();
        tcpReciever = new TcpReceiver();
        udpSender.Port = 10000;
        tcpReciever.Ip = "127.0.0.1";
        tcpReciever.Port = 8052;
        udpSender.Ip = "127.0.0.1";
        TcpReceiver.OnGetSettings += GetSettings;
        TcpReceiver.OnRequestUnits += RequestUnits;
        TcpReceiver.OnMoveUnits += MoveUnits;
        MoveController.OnUpdateMap += UpdateMap;
        tcpReciever.Start();
        udpSender.Start();
    }

    public void Stop()
    {
        Console.WriteLine("exit");
        tcpReciever.Stop();
        udpSender.Stop();
    }

    private void UpdateMap()
    {
        udpSender.SendUnitsUpdate(map.Units);
    }

    private void GetSettings(byte[] bytes) {
            int minsize = BitConverter.ToInt32(bytes, 0);
            int maxsize = BitConverter.ToInt32(bytes, sizeof(Int32));
            int maxunitssize = BitConverter.ToInt32(bytes, sizeof(Int32) * 2);
            map = new Map(minsize, maxsize, maxunitssize);
            moveController = new MoveController(map);
            var bytesback = BitConverter.GetBytes(map.Size);
            tcpReciever.SendData(bytesback);
    }

    private void RequestUnits()
    {
        var bytes = Utils.UnitsToByteArray(map.Units);
        tcpReciever.SendData(bytes);
    }

    private void MoveUnits(byte[] bytes)
    {
        var unit = Utils.ByteArrayToUnit(bytes);
        if (unit != null) {
            var msg = Encoding.ASCII.GetBytes("OK");
            tcpReciever.SendData(msg);
        }
        moveController.MoveInit(unit);
    }

    private void Stop(object sender, EventArgs e)
    {
        Stop();
    }
}

